import React, { Component } from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

const options = {
    chart: {
        zoomType: 'x'
    },
    title: {
        text: 'BTCUSD Crypto Chart'
    },
    legend: {
        enabled: false
      },
    plotOptions: {
        area: {
            fillColor: {
                linearGradient: {
                    x1: 0,
                    y1: 0,
                    x2: 0,
                    y2: 1
                },
                stops: [
                    [0, Highcharts.getOptions().colors[0]],
                    [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                ]
            },
            marker: {
                radius: 2
            },
            lineWidth: 1,
            states: {
                hover: {
                    lineWidth: 1
                }
            },
            threshold: null
        }
    },



    series: [
        {
            type: 'area',
            data: [1, 2, 1, 4, 3, 6, 5, 6, 6, 7, 7, 10, 11, 5, 6, 7, 8, 7,]
        }
    ]
};

class ChartPage extends Component {
    render() {
        return (
            <div>
                <HighchartsReact highcharts={Highcharts} options={options} />
            </div>
        )
    }
}

export default ChartPage;